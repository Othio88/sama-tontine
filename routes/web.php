<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TontineController;
use App\Http\Controllers\EcheanceController;
use App\Http\Controllers\AdhesionTontineController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Pages d'accueil est les elements
Route::get('/',[TontineController::class ,'listeDetontineAdherer']);
Route::get('/journalier',[TontineController::class, 'journalier']);
Route::get('/hebdomadaire',[TontineController::class, 'hebdomadaire']);
Route::get('/mensuelle',[TontineController::class, 'mensuelle']);
Route::get('/annuelle',[TontineController::class, 'annuelle']);

Route::view('/adherent/qui-sommes-nous','informations.qui-sommes-nous');



//Route::post('/password/{id}', [UserController::class, 'update']);
//Route::get('/modifier-passe/{id}', [UserController::class, 'modifierMotdepasse']);

//gerer les tontines

Route::middleware(['auth'])->group(function (){


Route::view('/profil-image-update','utilisateur.modifier-profil');
Route::post('/profil-update', [UserController::class, 'update']);
Route::post('/profil-image-update', [UserController::class, 'updateImage']);


Route::get('/utilisateur/profil/',[TontineController::class, 'pageDaccueilUser']);
Route::get('/adherent/tontines', [TontineController::class , 'listetontines']);
Route::get('/adherent/liste-tontine', [TontineController::class , 'listertontineCreer']);
Route::get('/adherent/ajouter-tontine', [TontineController::class , 'ajoutertontine']);
Route::post('/adherent/ajouter-tontine', [TontineController::class, 'creertontine']);
Route::get('/adherent/supprimer-tontine/{id}',[TontineController::class, 'supprimertontine']);
Route::patch('/adherent/modifier-tontine/{id}', [TontineController::class, 'modificationtontine']);
Route::get('/adherent/modifier-tontine-verif/{id}',[TontineController::class, 'modifiertontine']);
Route::post('/tontine-image-update/{id}', [TontineController::class, 'updateImageTontine']);



//Routes pour les Adhesions
Route::get('/adherent/adhesion-tontine/{id}',[AdhesionTontineController::class, 'adhesionTontineVerif']);
Route::post('/adherent/adhesion-tontine/{id}',[AdhesionTontineController ::class, 'adhesionTontine']);
Route::get('/adherent/tontine-adherer',[AdhesionTontineController ::class, 'lestontinesadherer']);
Route::get('/adherent/participant/{id}',[AdhesionTontineController ::class, 'lesparticipants']);
Route::get('/adherent/retirer-participant/{id}',[AdhesionTontineController::class, 'supprimerparticipant']);
//Route::get('/adherent/se-retier/{id}',[AdhesionTontineController::class, 'seRetirerAdhesion']);



//Routes pour les echeances et paiement
Route::get('/echeance/generer/{id}',[EcheanceController::class, 'gererecheance']);
Route::get('/etat-paiement/{id}',[EcheanceController::class, 'etatpaiement']);
Route::get('/payer-echeance/{idT}',[EcheanceController::class, 'cotisation']);



//Routes Administrateurs
Route::get('/admin/profil/',[AdminController::class, 'details']);
Route::get('/les-Utilisateurs',[AdminController::class, 'lesUsers']);
Route::get('/les-Tontines',[AdminController::class, 'lesTontines']);
Route::get('/sup-Utilisateurs/{id}',[AdminController::class, 'supprimerUser']);
Route::get('/sup-Tontines/{id}',[AdminController::class, 'supprimerTont']);


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

});

//Routes pour adhesion rapide depuis la page d'accueil
Route::get('/adherer/tontine-selectioner/{id}',[AdhesionTontineController::class, 'verificationAdhesion']);

Auth::routes();





