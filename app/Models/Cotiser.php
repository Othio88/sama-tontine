<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotiser extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_Echeance',
        'id_Adherent',
    ];
}
