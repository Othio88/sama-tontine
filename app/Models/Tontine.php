<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tontine extends Model
{
    use HasFactory;


    protected $fillable = [
        'nomtontine',
        'periodicite',
        'dateDeb',
        'id_responsable',
        'versement',

    ];

    public function users ()
    {
        return $this->belongsToMany('App\User');
    }

}

