<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
class UserController extends Controller
{
    //

    public function update(Request $request){
        //validation rules
        if ($request->input('email') == Auth::user()->email) {
            $data = $request->validate([
                'nom' =>'required|min:2|string|max:255',
                'prenom'=>'required|min:2|string|max:255',
                'adresse' =>'required|min:4|string|max:255',
                'email'=>'required|email|string|max:255',
                'password'=> ['required', 'string', 'min:8']
            ]);
        }else{
            $data = $request->validate([
                'nom' =>'required|min:2|string|max:255',
                'prenom'=>'required|min:2|string|max:255',
                'adresse' =>'required|min:4|string|max:255',
                'email'=>'required|email|string|max:255|unique:users',
                'password'=> ['required', 'string', 'min:8']
            ]);
        }

        if (Hash::check($request['password'], Auth::user()->password) ){
            #
            # code...
            $user = Auth::user();
            $user->nom = $request['nom'];
            $user->prenom = $request['prenom'];
            $user->adresse = $request['adresse'];
            $user->email = $request['email'];
            $user->save();

           // toastr()->success('profil modifié');

           return view("layouts.dashboard");

        }else{


           // toastr()->error('Mot de passe de l\'utilisateur incorrect');
            return back();
        }

        /*
        */

    }






    public function updateImage(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            # code...
            $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imageName = time().'.'.$request->image->extension();
       $request->image->move(public_path('images-profil'), $imageName);
        $user->image = $imageName;
        $user->save();

        return back();

        }


        //toastr()->success('Photo de profil modifiée');

    }





   /* public function modifierMotdepasse($id)
    {
        $user = User::findOrfail($id);

        //dd( $user);
        return view('auth.passwords.reset',compact('user'));
    }*/
}
