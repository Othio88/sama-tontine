<?php

namespace App\Http\Controllers;

use App\Models\Participer;
use App\Models\Tontine;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdhesionTontineController extends Controller
{

    public function lestontinesadherer()
    {
        $id=Auth::user()->id;
       $tontines = DB::table('tontines')
                            ->join('participers','participers.idTontine','=','tontines.id')
                            ->join('users', 'users.id','=','participers.idAdherent')
                            ->where('participers.idAdherent','=',$id)
                            ->select('tontines.*')
                            ->get();
       //dd($tontines);
           return view('GererTontine.pagetontineadherer', compact('tontines'));

    }



    public function adhesionTontineVerif($id_tontine)
    {
       $tontines = Tontine ::findOrFail($id_tontine);

       //dd( $tontines->id);

       $adheres = DB::table('tontines')->join('participers','participers.idTontine','=','tontines.id')
                                       ->join('users','users.id','=','participers.idAdherent')
                                       ->Where('participers.idTontine','=',$tontines->id)
                                       ->where('participers.idAdherent','=',Auth::user()->id)
                                       ->select('participers.id')
                                       ->first();

                                       //dd($adheres);
          if($adheres != Null)
          {
            toastr()->error('Vous ne pouvez pas adherer plusieur fois dans une meme tontine');
            return back();
          }else{

            return view('GererTontine.adherer-tontine', compact('tontines'));
          }

        //  toastr()->error('une meme tontine');


    }


    public function adhesionTontine(Request $request , $id)
    {
       $tontines = Tontine::findOrFail($id);

       //dd($tontines->id);
       $data = $request->validate(['montant' =>'required']);

     if($data['montant'] = $tontines->versement)
     {

            $participation = new Participer;
            $participation->montant = $data['montant'];
            $participation->idTontine = $tontines->id;
            $participation->idAdherent = Auth::user()->id;

            $participation->save();

            toastr()->success('Adhesion effectuee avec succés');
            return redirect('/adherent/tontine-adherer');
           // dd($participation);


     }else{
        toastr()->error('Vous devez respecter la somme demander');
        return view('utilisateur.profil');
    //dd($tontines->versement);
    }

    }



    public function lesparticipants($idtontine)
    {
        $tontines = Tontine ::findOrFail($idtontine);

        $participants = DB::table('tontines')
                            ->join('participers','participers.idTontine','=','tontines.id')
                            ->join('users','users.id','=','participers.idAdherent')
                            ->where('participers.idTontine','=',$tontines->id)
                            ->get();

        $tontines = DB::table('tontines')
                            ->join('participers','participers.idTontine','=','tontines.id')
                            ->join('users','users.id','=','participers.idAdherent')
                            ->where('participers.idTontine','=',$tontines->id)
                            ->select('tontines.nomtontine')
                            ->first();
          //dd( $tontines);

        if(count($participants)< 1)
        {
            toastr()->error('Il n\'y a pas encore de participants');
            return back();
        }
       // dd(  $participants);
        return view('GererTontine.liste-participant',compact('participants','tontines'));
    }


    public function supprimerparticipant($id)
    {

        $participants= Participer::findOrfail($id);
//dd( $participants);
        dd($participants);
        $participants->delete();
        toastr()->success('Participant supprimé avec succee');
            return back();
    }


    public function seRetirerAdhesion($id)
    {

        $participants= Participer::findOrfail($id);

        $tontine = DB::table('tontines')->join('participers','participers.idTontine','=','tontines.id')
        ->join('users','users.id','=','participers.idAdherent')
        ->where(['participers.idAdherent'=>Auth::user()->id])->select('users.*')->first();

        dd( $tontine);
        $participants->delete();

        toastr()->success('retirer avec succee');
            return back();
    }







    public function verificationAdhesion($id)
    {
        $tontine = Tontine::findOrfail($id);

        if (Auth::guest()){


            toastr()->info('Connectez vous dabord...');

           return redirect(route('login') );


        }


    }





}
