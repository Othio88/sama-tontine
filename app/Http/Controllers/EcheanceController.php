<?php

namespace App\Http\Controllers;

use App\Models\Cotiser;
use App\Models\Echeance;
use Illuminate\Support\Facades\DB;
use App\Models\Tontine;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EcheanceController extends Controller
{
    //

    public function gererecheance($id)
    {
       $tontine = Tontine::findOrFail($id);
//dd($tontine->id);
       $echeance = DB::table('echeances')->where('id_tontine','=',$id)->first();
//dd ($echeance);

                if($tontine->periodicite == 'hebdomadaire')
                {
                    $periodicite = 7;
                }
                elseif($tontine->periodicite == 'mensuelle')
                {
                    $periodicite = 30;
                }
                elseif($tontine->periodicite == 'journalier')
                {
                    $periodicite = 1;
                }
                elseif($tontine->periodicite == 'annuelle')
                {
                    $periodicite = 365;
                }

                $date_echeance =Carbon::parse($tontine->dateDeb);

                if(!$echeance)
                {
                    for($i=1 ; $i<=$tontine->nb_echeance ; $i++ )
                    {


                      $echeance = new Echeance;
                      $echeance->id_tontine = $tontine->id;
                      $echeance->date=$date_echeance;
                      $echeance->numero = $i;
                      $echeance->save();

                      $date_echeance = $date_echeance->addDay($periodicite) ;
                    }

                    toastr()->success('Echeance generer avec succés');
                    return back();
                }
                else{

                    toastr()->error('Vous avez deja genere l\'echeance');
                    return back();

                }
    }



    public function etatpaiement($id)
    {
        //recuperation du tontine concerner
        $tontines = Tontine::findOrFail($id);

        //recup de la date debut du tontine
        $dateDebuts = Echeance::join('tontines','tontines.id','=','echeances.id_tontine')
                               ->where('echeances.id_tontine','=',$tontines->id)
                               ->select('echeances.date')->first();

   //dd($echeance1 );

   //recup des echeances du tontine
        $echeances = Echeance::join('tontines','tontines.id','=','echeances.id_tontine')
                               ->where('echeances.id_tontine','=',$tontines->id)
                               ->select('echeances.date')->get();

    // recupdes echeances de cette tontine qui ont ete payes
     $echeancespayer = Echeance::join('tontines','tontines.id','=','echeances.id_tontine')
                                ->join('cotisers','cotisers.idEcheance','=','echeances.id')
                                ->where('echeances.id_tontine','=',$tontines->id)
                                ->select('echeances.*')->get();
//dd( $echeancespayer);

         return view('GererTontine.etat-paiement', compact('echeances','tontines','dateDebuts','echeancespayer'));

    }


    public function cotisation($id)
    {
           $tontines = Tontine::findOrfail($id);
           //$echeancerecup = Echeance::findOrfail($idE);


           $datedeb = Carbon::parse($tontines->date);

           $echeances = Echeance::join('tontines','tontines.id','=','echeances.id_tontine')
           ->join('participers','participers.idTontine','=','tontines.id')
           ->join('users','users.id','=','participers.idAdherent')
           ->where('echeances.id_tontine','=',$tontines->id)
           ->select('echeances.*')->get();



             $n = count($echeances);

             $dateactu = Carbon::now()->format('Y-m-d');
            //$dateVerif = Carbon::parse(  $dateactu);

            for($i=0 ; $i<$n ; $i++ )
            {
              // dd( $echeances[$i]->id );
                $echeancesid = Echeance::join('tontines','tontines.id','=','echeances.id_tontine')
                ->join('participers','participers.idTontine','=','tontines.id')
                ->join('users','users.id','=','participers.idAdherent')
                ->join('cotisers','cotisers.idEcheance','=','echeances.id')
                ->where('cotisers.idEcheance','=',$echeances[$i]->id)
                ->select('echeances.*')->first ();

              //dd(  $echeancesid)

                   if($echeancesid == Null)
                   {
                           if( $dateactu< $echeances[$i]->date)
                            {
                                toastr()->error('La date pour pouvoir payer n\'est pas encore arrivee veillez patienter');
                                return back();
                            }
                            elseif( $dateactu > $echeances[$i]->date)
                            {

                                toastr()->error('La date est passee ');
                                return back();
                            }
                            else
                            {
                                //dd('dfghjk');
                                $cotisers = new Cotiser;
                                $cotisers->idEcheance = $echeances[$i]->id;
                                $cotisers->idAdherent = Auth::user()->id;
                                $cotisers->save();

                                toastr()->success('Cotisation Reussie');
                                return back();
                            }


                   }


                }


               // echo($echeances[$i]->date);



                    }

                }



