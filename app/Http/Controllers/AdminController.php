<?php

namespace App\Http\Controllers;

use App\Models\Tontine;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function details()
      {
    $users = User::where(['profil'=>'adherent'])->get()->count();

    $tontines = Tontine::get()->count();

    return view('admin.profil-admin', compact('users','tontines'));
   }




public function lesUsers()
{
    $users = User::where(['profil'=>'adherent'])->get();

    $nombre = count($users);

    return view('admin.liste-utilisateur', compact('users','nombre'));
}

public function lesTontines()
{
    $tontines = Tontine::get();

    $nombre = count($tontines);

    return view('admin.liste-tontine', compact('tontines','nombre'));
}

public function supprimerTont($id)
{
    # code...
    $tontine = Tontine::findOrFail($id);
    $tontine->delete();
    toastr()->success('Tontine supprimée avec succee');
        return back();
}

public function supprimerUser($id)
{
    # code...
    $users = User::findOrFail($id);
    $users->delete();
    toastr()->success('Tontine supprimée avec succee');
        return back();
}

}
