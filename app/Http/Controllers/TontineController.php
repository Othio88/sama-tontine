<?php

namespace App\Http\Controllers;

use App\Models\Echeance;
use App\Models\Tontine;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class TontineController extends Controller
{
    //les tontines creer pat le User
    public function listertontineCreer()
    {
        $tontines = DB::table('tontines')->where(['id_responsable'=>Auth::user()->id])->get();
        return view('GererTontine.lister-tontine', compact('tontines'));
    }

    //les Tontines qui sont disponible pas encore adherer
    public function listetontines()
    {
        $tontinesdispo = DB::table('tontines')
                           // ->join('participers','participers.idTontine','=','tontines.id')
                           // ->join('users','participers.idAdherent','=','users.id')
                           // ->where('participers.idTontine','=','tontines.id','and', 'participers.idAdherent','<>',Auth::user()->id)
                            ->get();

      // dd( $tontinesdispo);
        return view('GererTontine.tontines', compact('tontinesdispo'));
    }



   /* public function nombreTontineCreer()
    {
        $tontines1 = DB::table('tontines')->where(['periodicite','=','journalier'])->get();
        $nombre1 = count($tontines1);

        $tontines2 = DB::table('tontines')->where(['periodicite','=','hebdomadaire'])->get();
        $nombre2 = count($tontines2);

        $tontines3 = DB::table('tontines')->where(['periodicite','=','mensuelle'])->get();
        $nombre3 = count($tontines3);

        $tontines4 = DB::table('tontines')->where(['periodicite','=','annuelle'])->get();
        $nombre4 = count($tontines4);


       return view('index' , compact('tontines1 ','nombre2','nombre3','nombre4'));
    }
*/


     //Verification de l'ajout d'une tontine
    public function ajoutertontine(){
        $adherents = User ::where(['id' =>Auth::user()->id])->get();

       // dd($dateactuel);
        if (count($adherents) < 1) {
            //toastr()->error('Vous ne pouvez pas ajouter une gare sans un chef de gare');
            return back();
        }else{




        return view('GererTontine.ajouter-tontine', compact('adherents'));
        }

        }


             //l'ajout d'une tontine
            public function creertontine(Request $request){



                //$adherents = DB::table('tontines')->where(['id_responsable'=>$request->input('adherent')])->get();
                $adherents = $request->input('adherent');

                if ($adherents != Auth::user()->id) {

                   // toastr()->error('Remettre le champ');
                    return back();
                }else{


                   $data = $request->validate([
                        'nomtontine' =>'required|min:2|string|max:255',
                        'periodicite'=>'required',
                        'dateDeb' =>'required|date_format:d-m-Y',
                        'nb_echeance' =>'required',
                        'versement'=>'required',
                    ]);

                 //   $datefinish = Carbon::createFromFormat('d-m-Y', $data['dateFin']); //Carbon::parse($data['date_arrivee']);
                    $datedebut = Carbon::createFromFormat('d-m-Y', $data['dateDeb']); //Carbon::parse($data['date_arrivee']);

                   // dd( $datefinish);
                    $dateactuel = Carbon::now()->format('d-m-Y');


                  //  dd($dateactuel);
                    if ($datedebut->lt($dateactuel)) {
                        # code...
                        // proble
                        toastr()->error('Date de debut est inferieure a la date actuelle');
                        return back();
                    }


                    $tontine = new Tontine;
                    $tontine->nomtontine = $data['nomtontine'];
                    $tontine->periodicite = $data['periodicite'];
                    $tontine->dateDeb = $datedebut;
                  //  $tontine->dateFin =  $datefinish;
                    $tontine->nb_echeance = $data['nb_echeance'];
                    $tontine->versement = $data['versement'];
                    $tontine->id_responsable = Auth::user()->id;

                    $tontine->save();

                    toastr()->success('tontine  ajouter avec succès');

                    return back();

        }

    }




        public function modifiertontine($id){

            $laTontineChoisie = Tontine::findOrFail($id);

            $tontines= DB::table('tontines')->where(['id_responsable' => Auth::user()->id])->where('id','=',$laTontineChoisie->id)->first();
           //dd( $tontines);
            $adherents = DB::table('users')->where(['id' =>Auth::user()->id])->get();
           // dd($adherents, $tontines);
            if($tontines)
                return view('GererTontine.modifier-tontine', compact('adherents','tontines'));

            return abort(404);
        }




        public function modificationtontine(Request $request, $id){

            if (!$request->input('adherent') ) {

                //toastr()->error('Remettre le champ');
                return back();
            }else{

                $tontine = Tontine::findOrFail($id);
                $verif = $tontine->id  ===  (int) $request->input('adherent');

                //dd($user->id);

                if (!$verif) {
                    $data = $request->validate([
                        'nomtontine' =>'required|min:2|string|max:255',
                        'periodicite'=>'required',
                        'dateDeb' =>'required|date_format:d-m-Y',
                       // 'dateFin' =>'required|date_format:d-m-Y',
                        'nb_echeance' =>'required',
                        'versement'=>'required',


                    ]);
                   // dd( $data);

                }else{

                    $data = $request->validate([
                        'nomtontine' =>'required|min:2|string|max:255',
                        'periodicite'=>'required',
                        'dateDeb' =>'required|date_format:d-m-Y',
                        //'dateFin' =>'required|date_format:d-m-Y',
                        'nb_echeance' =>'required',
                        'versement'=>'required',

                            ]);



                     }


                   // $datefinish = Carbon::createFromFormat('d-m-Y', $data['dateFin']); //Carbon::parse($data['date_arrivee']);
                    $datedebut = Carbon::createFromFormat('d-m-Y', $data['dateDeb']); //Carbon::parse($data['date_arrivee']);

                   // dd( $datefinish);
                    $dateactuel = Carbon::now();
                  //  dd($dateactuel);
                    if ($datedebut->lt($dateactuel)) {
                        # code...
                        // proble
                        toastr()->error('Date de debut est inferieure a la date actuelle');
                        return back();
                    }
                  /*  else if ($datefinish->lt($datedebut)||$datefinish->eq($datedebut)) {
                        # code...
                        // proble
                        toastr()->error('Date de debut est superieur/egale a la date que doit terminer la tontine');
                        return back();
                    }else if ($datefinish->eq($datedebut)) {
                            # code...
                            // proble
                            toastr()->error('Date de debut ne peut pas etre egale a la date que doit terminer la tontine');
                            return back();

                    }else{*/



                    $tontine->nomtontine = $data['nomtontine'];
                    $tontine->periodicite = $data['periodicite'];
                    $tontine->dateDeb = $datedebut;
                  //  $tontine->dateFin = $datefinish;
                    $tontine->nb_echeance = $data['nb_echeance'];
                    $tontine->versement = $data['versement'];
                    $tontine->id_responsable = Auth::user()->id;
                   // dd($tontine);

                    $tontine->save();
               toastr()->success('tontine  modifiée ');

              return back();


        }
    }




//****************************DETAIL DES TONTINES******************************* */
    public function listeDetontineAdherer()
    {
                            $tontines = DB::table('tontines')
                            ->select('tontines.*')
                            ->get();

                            $nbr = count($tontines);

                            $tontines1 = DB::table('tontines')->where(['periodicite'=>'journalier'])->get();
                            $nombre1 = count($tontines1);

                            $tontines2 = DB::table('tontines')->where(['periodicite'=>'hebdomadaire'])->get();
                            $nombre2 = count($tontines2);

                            $tontines3 = DB::table('tontines')->where(['periodicite'=>'mensuelle'])->get();
                            $nombre3 = count($tontines3);

                            $tontines4 = DB::table('tontines')->where(['periodicite'=>'annuelle'])->get();
                            $nombre4 = count($tontines4);

                            $users = DB::table('users')
                            ->select('users.*')->where(['profil'=>'adherent'])->get();

                            $nbrUser = count($users);

                            $tontinesCreer = DB::table('tontines')->join('users','users.id','=','tontines.id_responsable')
                            ->select('tontines.*','users.*')
                            ->get();
                          //  dd($tontinesCreer);
                    return view('index' , compact('tontines','nombre1','nombre2','nombre3','nombre4','nbr','users','nbrUser','tontinesCreer'));

    }








   public function pageDaccueilUser()
    {
        $T_Creer = DB::table('tontines')->join('users','users.id','=','tontines.id_responsable')
        ->where(['users.id'=>Auth::user()->id])
        ->get()->count();

        $T_Adherer = DB::table('tontines')
        ->join('participers','participers.idTontine','=','tontines.id')
        ->join('users', 'users.id','=','participers.idAdherent')
        ->where(['participers.idAdherent'=>Auth::user()->id])
        ->get()->count();
       // dd( $tontinesCreer ,   $tontinesAdherer );
   $date = Carbon::now();


       $T_Encour = $tontines = DB::table('tontines')
       ->join('participers','participers.idTontine','=','tontines.id')
       ->join('users', 'users.id','=','participers.idAdherent')
       ->where(['participers.idAdherent'=>Auth::user()->id])
       ->where('tontines.dateDeb','<',$date)
       ->select('tontines.dateDeb')
       ->get();

         //$verif = Carbon::parse($data['date_arrivee']);

       //dd( $T_Encour );

        return view('utilisateur.profil', compact('T_Creer','T_Adherer'));

    }





//*****************************Lister en fonction des periodicite*****************************\\
    public function journalier()
    {
        $journaliers = DB::table('tontines')
        ->where('tontines.periodicite','=','journalier')
        ->select('tontines.*')
        ->get();

        return view('index', compact('journaliers'));
    }

    public function hebdomadaire()
    {
        $hebdomadaires = DB::table('tontines')
                            ->where('tontines.periodicite','=','hebdomadaire')
                            ->select('tontines.*')
                            ->get();

        return view('index', compact('hebdomadaires'));
    }

    public function mensuelle()
    {
        $mensuelles = DB::table('tontines')
        ->where('tontines.periodicite','=','mensuelles')
        ->select('tontines.*')
        ->get();

        return view('index', compact('mensuelles'));
    }

    public function annuelle()
    {

        $annuelles = DB::table('tontines')
        ->where('tontines.periodicite','=','annuelle')
        ->select('tontines.*')
        ->get();
        return view('table', compact('annuelles'));
    }

//************************************************************************ */






//************************CRUD Tontine******************************* */

        public function supprimertontine($idAdherent)
        {
            # code...
            $tontine = Tontine::findOrFail($idAdherent);
            $tontine->delete();
            toastr()->success('Tontine supprimée avec succee');
                return back();
        }


        public function updateImageTontine(Request $request , $id)
        {
            $tontine = Tontine::findOrfail($id);
           // dd($tontine);
            if ($tontine) {
                # code...
                $request->validate([
                'imagetontine' => 'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
            ]);

            $imageName = time().'.'.$request->imagetontine->extension();
           $request->imagetontine->move(public_path('images-profil'), $imageName);
            $tontine->imagetontine = $imageName;
            $tontine->save();
            toastr()->success('Image ajoutee');
            return back();

            }
            //toastr()->success('Photo de profil modifiée');
        }
//********************************************************************************* */



}
