(function($) {
    "use strict"

    jQuery('.mydatepicker, #datepicker').datepicker({dateFormat: "dd-mm-dd", timeFormat: "hh:mm:ss"});
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        jQuery('#datepicker-inline').datepicker({
            todayHighlight: true
        });
        
})(jQuery);

