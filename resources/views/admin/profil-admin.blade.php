@extends('layouts.dashboard')


@section('Content')


<div class="container-fluid py-4" style="margin-top: 2% ; text-align:center" >
    <div class="row">
      <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Nombres  d'Utilisateurs</p>


                 <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">{{$users}}</span>
                  </h5>

                 <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-sm font-weight-bolder"></span>
                  </h5>

                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Nombres  Tontines</p>

                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder"> {{$tontines}}</span>
                  </h5>

                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-sm font-weight-bolder"></span>
                  </h5>

                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


@endsection
