@extends('layouts.dashboard')


@section('Content')



  <div class="row my-4">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
      <div class="card">
        <div class="card-header pb-0">
          <div class="row">
            <div class="col-lg-6 col-7">
              <h6>Les Utilisateurs</h6>
              <p class="text-sm mb-0">
                <i class="fa fa-check text-info" aria-hidden="true"></i>
               @if($nombre)
               <span class="text-success text-sm font-weight-bolder">{{$nombre}}</span>@endif qui existe pour l'instant dans la base de donnees
              </p>
            </div>
          </div>
        </div>
        <div class="card-body px-0 pb-2">
          <div class="table-responsive">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity">Photo</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nom</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Prenom</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Adresse</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email</th>

                </tr>
              </thead>
              <tbody>
                @foreach($users as $user )
                <tr>

                  <td>
                    <div class="d-flex px-2 py-1">
                        <div class="avatar me-3">
                            @if ($user->image == null)
                            <img height="50" width="10" src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="kal" class="border-radius-lg shadow">
                        @else
                           <img height="50" width="10" src="{{ asset('images-profil/'. $user->image) }}" alt="kal" class="border-radius-lg shadow">
                        @endif
                          </div>
                        </td>
                        <td>
                            <div class="avatar-group mt-2">
                      <div class="d-flex flex-column justify-content-center"  style="margin-left: 3%">
                        <h6 class="mb-0 text-sm">{{$user->nom}}</h6>
                      </div>
                            </div>
                    </div>
                  </td>
                  <td>
                    <div class="avatar-group mt-2">
                        <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">{{$user->prenom}}</h6>
                          </div>
                    </div>
                  </td>

                  <td >
                    <div class="avatar-group mt-2">
                        <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">{{$user->adresse}}</h6>
                        </div>
                  </td>

                  <td class="align-middle text-center text-sm">
                    <h6 class="mb-0 text-sm">{{$user->email}}</h6>
                  </td>

                  <td class="align-middle text-center text-sm">
                    <a class="btn btn-outline-danger btn-sm mb-0 me-3" href="/sup-Utilisateurs/{{$user->id}}">Retirer-User</a>
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection
