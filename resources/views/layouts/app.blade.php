
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/fav.png">
  <link rel="icon" type="image/png" href="../assets/img/fav.png">
  <title>
    SamaTontine
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />

<!-- Bootstrap core CSS -->


<!-- Bootstrap core CSS -->



    <!-- Additional CSS Files -->


    <link rel="stylesheet" href="thio/css/animated.css">
    <link rel="stylesheet" href="thio/css/owl.css">




</head>

<body class="">
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-15">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid pe-0">

            <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " href="/">
                <img src="../assets/img/logotontine.png" width="300" height="auto" alt="..." style="margin-bottom: 3.5%" >
            </a>
            <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon mt-2">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </span>
            </button>

            <div class="collapse navbar-collapse" id="navigation">
              <ul class="navbar-nav mx-auto ms-xl-auto me-xl-7">

                @if (Auth::user())

                <li class="nav-item"style="margin-right:10%">
                  <a class="nav-link d-flex align-items-center me-2 " aria-current="page" href="/utilisateur/profil">

                   Accueil
                  </a>
                </li>
                <li class="nav-item">



                </li>

                @endif


                @if (Auth::guest())
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="/">

                   Accueil
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="/adherent/qui-sommes-nous">

                    Qui sommes nous?
                  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link me-2" >


                    </a>
                  </li>

              </ul>

              <li class="nav-item d-flex align-items-center">
                <a class="btn btn-round btn-sm mb-0 btn-outline-primary me-2"  href="{{ route('login') }}">Connecter</a>
              </li>
              <ul class="navbar-nav d-lg-block d-none">
                <li class="nav-item">
                  <a href="{{ route('register') }}" class="btn btn-sm btn-round mb-0 me-1 bg-gradient-dark">S'Inscrire</a>
                </li>
                @else
                <a class="btn btn-outline-primary btn-sm mb-0 me-3" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    {{__('Deconnecter')}}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
                </form>
              @endif
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>


  <main class="main-content  mt-0">
    @yield('contentPage')
    <section>

      <div class="page-header min-vh-5">

        <div class="container">


          <div class="row">

            <div class="col-xl-6 col-lg-5 col-md-6 d-flex flex-column mx-auto">

              <div class="card card-plain mt-8">


                <div class="card-header pb-0 text-left bg-transparent" style="text-align: center;">
                   @if (Auth::guest())

                  @else
                  <h3 class="font-weight-bolder text-info text-gradient">Yagui Cii "SAMA-TONTINE"</h3>
                  @endif
                </div>

                <div class="card-body">

                    @yield('Content')
            @yield('ContentA')


                </div>
              </div>
            </div>
            @yield('content')
            @yield("Contentphoto")
            <!--<div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../assets/img/curved-images/curved6.jpg')"></div>
              </div>
            </div>-->
            @yield("Contentindex")
          </div>
        </div>
      </div>
    </section>
  </main>


  <footer>
    <div class="container">
        <div class="row">
          <div class="col-8 mx-auto text-center mt-1">
            <p class="mb-0 text-secondary">
              Copyright © <script>
              //  document.write(new Date().getFullYear())
              </script> Développer par Mamour THIOMBANE | Abou Bacre SALL  année univérsitaire 2021-2022
            </p>
          </div>
        </div>
      </div>
  </footer>
  <!-- -------- START FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <footer class="footer py-5">

  </footer>
  <!-- -------- END FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <!--   Core JS Files   -->
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="../assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>


  <!-- Scripts -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="thio/js/owl-carousel.js"></script>
  <script src="thio/js/animation.js"></script>
  <script src="thio/js/imagesloaded.js"></script>
  <script src="thio/js/custom.js"></script>

  <script>
  // Acc
    $(document).on("click", ".naccs .menu div", function() {
      var numberIndex = $(this).index();

      if (!$(this).is("active")) {
          $(".naccs .menu div").removeClass("active");
          $(".naccs ul li").removeClass("active");

          $(this).addClass("active");
          $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

          var listItemHeight = $(".naccs ul")
            .find("li:eq(" + numberIndex + ")")
            .innerHeight();
          $(".naccs ul").height(listItemHeight + "px");
        }
    });
  </script>

</body>

</html>
