@extends('layouts.app')

@section("Content")

<h3 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h3>

<h1>Qui sommes nous</h1>

<div class="row">
    <div class="card" style="width: 18rem;">
        <img src="../assets/img/sall.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">M. Abou Bacre SALL</h5>
          <p class="card-text">Etudiant en troisiéme années en Developement et Administration d'Application a l'Université Alioune DIOP de Bambey</p>
          <a href="mailto:sallabou703@gmail.com" class="btn btn-primary">Plus d'informations</a>
        </div>
      </div>

      <div class="card" style="width: 18rem;">
        <img src="../assets/img/thio.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">M. Mamour THIOMBANE</h5>
          <p class="card-text">Etudiant en troisiéme années en Developement et Administration d'Application a l'Université Alioune DIOP de Bambey</p>
          <a href="mailto:thiombanemamour651@gmail.com" class="btn btn-primary">Plus d'informations</a>
        </div>
      </div>


</div>


@endsection

@section('Contentphoto')


 <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../assets/img/curved-images/curved-6.jpg')"></div>
              </div>
            </div>

@endsection

