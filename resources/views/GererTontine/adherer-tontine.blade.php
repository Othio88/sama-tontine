@extends("layouts.dashboard")



@section("Content")
<div class="container">
    <div class="row justify-content-center">
                <div class="card-body">
                    @if($tontines)

                    <h3>La somme a respecter est de {{$tontines->versement}} Fcfa </h3>

                    <div>

                        <form role="form" method="POST" action="/adherent/adhesion-tontine/{{$tontines->id}}">
                            @csrf

                            <label>Montant</label>
                            <div class="mb-3">
                                <input id="montant" type="montant" class="form-control @error('montant') is-invalid @enderror" name="montant" value="{{ old('montant') }}" placeholder="Saisir le montant" required autocomplete="montant" autofocus>

                                @error('montant')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                             <div class="text-center">
                                <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Adherer</button>
                              </div>



                         </form>
                        @endif
                    </div>
                </div>
</div>
@endsection





