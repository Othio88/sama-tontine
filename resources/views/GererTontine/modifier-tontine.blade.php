@extends('layouts.dashboard')

@section('Content')
    <div class=" pt-3 mt-5" >
            <div class="toolbar container-fluid" role="toolbar">

                    <h3 class="text-center">Modifier Tontine</h3>

            </div>


            <div style="text-align: center">
                @if($tontines->imagetontine == NULL)
                <img alt="" class="rounded-circle mt-4" width="200px" height="200px"  src="{{url('assets/img/curved-images/curved-11.jpg')}}">
                @else
                <img alt="profil image" width="150px" height="200px" class="rounded-circle mt-4" src="{{ asset('images-profil/'. $tontines->imagetontine) }}">
                @endif

                <form style="text-align: center" action="{{ url('tontine-image-update/'.$tontines->id) }}" method="POST" enctype="multipart/form-data">

                    @csrf
                                <label>Une Photo refletant la tontine</label>
                                <input class="form-control-file @error('imagetontine') is-invalid @enderror" type="file" name="imagetontine" style="margin-left: 41%"/>
                                @error('imagetontine')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                             <div style="margin-top: 3%">
                                <input type="submit" class="btn btn-primary" value="Changer"/>
                             </div>

                             <hr class="horizontal dark my-3">

                </form>


            </div>

                                <div class="card-body" style="margin-left: 10%">
                                    <div class="form-validation">

                                        <form action="{{ url('/adherent/modifier-tontine/'.$tontines->id) }}"  method="POST">
                                            @method('PATCH')
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-md-10">
                                                    <label>Nom</label>
                                                    <input type="text" name="nomtontine" class="form-control  @error('nomtontine') is-invalid @enderror" required value="{{$tontines->nomtontine}}"  placeholder="Nom">
                                                    @error('nomtontine')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>
                                                <div class="form-group col-md-10">
                                                    <label>Periodiciter</label>
                                                    <input type="text" name="periodicite" class="form-control  @error('periodicite') is-invalid @enderror" required value="{{$tontines->periodicite }}"  placeholder="Periodicite">
                                                    @error('periodicite')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>
                                                <div class="form-group col-md-10">
                                                    <label>Date début</label>
                                                    <input type="text" id="dateModifDebut" name="dateDeb" class="form-control  @error('dateDeb') is-invalid @enderror" required value="{{ $tontines->dateDeb }}"  placeholder="Date Debut">
                                                    @error('dateDeb')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <label>Versement</label>
                                                    <input type="text" name="versement" class="form-control  @error('versement') is-invalid @enderror" required value="{{$tontines->versement }}"  placeholder="Le versement">
                                                    @error('versement')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <label>Nombre D'echeance</label>
                                                    <input type="text" name="nb_echeance" class="form-control  @error('nb_echeance') is-invalid @enderror" required value="{{$tontines->nb_echeance }}"  placeholder="Nombre d'echeance">
                                                    @error('nb_echeance')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>



                                            <div class="form-group col-md-10">
                                                <label>Responsable</label>
                                                <select class="form-control @error('adherent') is-invalid @enderror" id="adherent" name="adherent">
                                                        @foreach ($adherents as $adherent)

                                                            <option value="{{$adherent->id}}">{{$adherent->prenom}}  {{$adherent->nom}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('adherent')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                            <button type="submit" class="btn btn-primary btn-rounded">Enregistrer</button>
                                        </form>









                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
<!-- #/ container -->



@endsection

@section('Contentphoto')
 <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../assets/img/curved-images/curved-8.jpg')"></div>
              </div>
            </div>

@endsection
