

@extends('layouts.dashboard')


@section('Content')
<div class=" pt-3 mt-5" >
    <div class="toolbar container-fluid" role="toolbar">

            <h3 class="text-center">Les Tontines que j'ai adhérer</h3>

    </div>






 <table class="table align-items-center mb-20">
    <thead>
      <tr>
        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nom Tontine</th>
        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Periodicité</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">date debut</th>
        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre d'écheance</th>
        <th class="text-secondary opacity-7"> </th>
      </tr>
    </thead>


    <tbody>
        @foreach ($tontines as $tontine)
          <tr>
            <td>
                <div class="d-flex px-0 py-1">
                    @if ($tontine->imagetontine == Null)
                        <div  class="avatar me-3">
                            <img src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="kal" class="border-radius-lg shadow">
                        </div>
                    @else
                      <div class="avatar me-3" >
                            <img src="{{ asset('images-profil/'. $tontine->imagetontine) }}" alt="kal" class="border-radius-lg shadow">

                        </div>
                    @endif
                  <div class="d-flex flex-column justify-content-center">

                  </div>
                </div>
              </td>




            <td>
              <div class="d-flex px-2 py-1">
                <div>
                    <!--nom-->{{$tontine->nomtontine}}
                </div>
                <div class="d-flex flex-column justify-content-center">

                </div>
              </div>
            </td>


            <td>
                <h6 class="mb-0 text-sm">  <!--periodicite-->{{$tontine->periodicite}}
            </td>
            <td class="align-middle text-center text-sm">
                <h6 class="mb-0 text-sm">  <!--datedeb-->{{$tontine->dateDeb}}
            </td>


            <td class="align-middle text-center text-sm">
                <h6 class="mb-0 text-sm"> <!--echeance-->{{$tontine->nb_echeance}}
            </td>

            <td class="align-middle text-center text-sm">
                <a  class="btn bg-gradient-secondary mt-3 btn-sm" href="/etat-paiement/{{$tontine->id}}">l'etat paiement</a>
            </td>


          </tr>
          @endforeach
        </tbody>
      </table>






@endsection




