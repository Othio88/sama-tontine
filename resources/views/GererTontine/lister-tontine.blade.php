

@extends('layouts.dashboard')


@section('Content')
<div class=" pt-2 mt-3" >
    <div class="toolbar container-fluid" role="toolbar">

            <h3 class="text-center">Liste de mes Tontines</h3>

    </div>



              <table class="table align-items-center mb-20">
                <thead>
                  <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Image</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nom Tontine</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Periodicité</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">date debut</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre echeance</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Versement</th>
                    <th class="text-secondary opacity-7"> </th>
                  </tr>
                </thead>
                <tbody>





                    @foreach ($tontines as $tontine)


                  <tr>

                    <td>
                        <div class="d-flex px-2 py-1">
                            @if ($tontine->imagetontine == Null)
                                <div  class="avatar me-3">
                                    <img src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="kal" class="border-radius-lg shadow">
                                </div>
                            @else
                              <div class="avatar me-3" >
                                    <img src="{{ asset('images-profil/'. $tontine->imagetontine) }}" alt="kal" class="border-radius-lg shadow">

                                </div>
                            @endif
                          <div class="d-flex flex-column justify-content-center">

                          </div>
                        </div>
                      </td>
                    <td>
                      <div class="d-flex px-2 py-1">
                        <div>
                             {{$tontine->nomtontine}}
                        </div>
                        <div class="d-flex flex-column justify-content-center">

                        </div>
                      </div>
                    </td>


                    <td>
                        <h6 class="mb-0 text-sm"> {{$tontine->periodicite}}
                    </td>
                    <td class="align-middle text-center text-sm">
                        <h6 class="mb-0 text-sm"> {{$tontine->dateDeb}}
                    </td>



                    <td class="align-middle text-center">
                        <h6 class="mb-0 text-sm">{{$tontine->nb_echeance}}
                    </td>
                    <td class="align-middle text-center">
                        <h6 class="mb-0 text-sm">{{$tontine->versement}}
                    </td>


                    <td class="align-middle">
                        <a class="btn bg-gradient-info  mt-3 btn-sm" href="/adherent/modifier-tontine-verif/{{$tontine->id}}">modifier</a>
                   </td>

                   <td class="align-middle">
                    <a class="btn bg-gradient-danger mt-3 btn-sm" href="/adherent/supprimer-tontine/{{$tontine->id}}">Supprimmer</a>
                   </td>
                   <td class="align-middle">
                    <a class="btn bg-gradient-warning mt-3 btn-sm" href="/adherent/participant/{{$tontine->id}}">Participants</a>
               </td>

                    <td class="align-middle">
                         <a class="btn bg-gradient-success mt-3 btn-sm" href="/echeance/generer/{{$tontine->id}}">Generer l'echeance</a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
</div>

@endsection




