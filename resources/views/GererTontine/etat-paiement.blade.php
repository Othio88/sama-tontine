

@extends('layouts.dashboard')


@section('Content')

<P><HR NOSHADE></P>
@if ($tontines)
<h3 class="text-center">Detail du Tontine :&nbsp; {{$tontines->nomtontine}}</h3>

<h6 class="text-center">Description {{$tontines->nomtontine}}</h6>
@if($dateDebuts)

<P><HR NOSHADE></P>
    <div style="margin-left: 3%">
        <ul >
            <li>
                <h6 style="margin-bottom: 1%">Date du debut</h6>
                <span class="btn bg-gradient-info  mt btn-sm">{{$dateDebuts->date}}</span>
            </li>

            <li>
               <h6 style="margin-bottom: 1%">Nombre d'echeance prevues</h6>
                @foreach ($echeances as $echeance)
                   <span class="btn bg-gradient-warning mt btn-sm" style="margin-left:0.2%">{{$echeance->date}}</span>
                @endforeach
            </li>

        </ul>

    </div>

                                                    <P><HR NOSHADE></P>
                                        <h4 style="text-align: center">Participant</h4>
                                                    <P><HR NOSHADE></P>
                                        <div style="margin-left: 3%">
                                            <h5 >Prenom/Nom : {{Auth::user()->prenom}} {{Auth::user()->nom}}</h5>
                                            <h5>Cotisation:&nbsp;{{$tontines->versement}} FCFA </h5>
                                            <h5>Les echeances deja payer</h5>
                                            <img src="{{url('cotiser.png')}}" alt="">
                                        </div>

                                        <div style="margin-left: 3%">
                                            @foreach ($echeancespayer as $payer)
                                            <span class="btn bg-gradient-success mt btn-sm" style="margin-left:0.2%">{{$payer->date}}</span>
                                            @endforeach
                                        </div>

                                        <div style="margin-left: 3%">
                                        <a class="btn btn-outline-success btn-sm mb-0 me-3" href="/payer-echeance/{{$tontines->id}}">Payer cheance</a>
                                        </div>


  @endif
  @endif

@endsection




