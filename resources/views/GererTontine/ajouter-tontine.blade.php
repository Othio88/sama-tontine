@extends('layouts.dashboard')



@section('Content')
<div class="container">
    <div class="row justify-content-center">
           <div class=" pt-3 mt-5" >
                                <div class="toolbar container-fluid" role="toolbar">

                                        <h3 class="text-center">Ajouter un Tontine</h3>

                                 </div>

                                <div class="card-body">
                                    <div class="form-validation">

                                        <form action="{{ url('/adherent/ajouter-tontine') }}" method="POST">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-md-10">
                                                    <label>Nom tontine</label>
                                                    <input type="text" name="nomtontine" class="form-control  @error('nomtontine') is-invalid @enderror" required value="{{ old('nomtontine') }}"  placeholder="Nom">
                                                    @error('nomtontine')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>
                                                <div class="form-group col-md-10">
                                                    <label>Periodiciter</label>
                                                    <select class="form-control @error('periodicite') is-invalid @enderror" id="periodicite" name="periodicite">
                                                        <option value="journalier">journalier</option>
                                                        <option value="mensuelle">mensuelle</option>
                                                        <option value="hebdomadaire">hebdomadaire</option>
                                                        <option value="annuelle">annuelle</option>
                                                    </select>

                                                    @error('periodicite')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <label>Date début du tontine</label>
                                                        <input type="text" id="datedebutTontine" name="dateDeb" class="form-control  @error('dateDeb') is-invalid @enderror" required value="{{ old('dateDeb') }}"  placeholder="Date Debut">
                                                    @error('dateDeb')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <label>Versement</label>
                                                    <input type="text" name="versement" class="form-control  @error('versement') is-invalid @enderror" required value="{{ old('versement') }}"  placeholder="Le versement">
                                                    @error('versement')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                                <div class="form-group col-md-10">
                                                    <label>Nombre D'echeance</label>
                                                    <input type="text" name="nb_echeance" class="form-control  @error('nb_echeance') is-invalid @enderror" required value="{{ old('nb_echeancess') }}"  placeholder="Nombre d'echeance">
                                                    @error('nb_echeance')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                     @enderror
                                                </div>

                                            <div class="form-group col-md-10">
                                                <label>Responsable</label>
                                                <select class="form-control @error('adherent') is-invalid @enderror" id="adherent" name="adherent">
                                                        @foreach ($adherents as $adherent)

                                                            <option value="{{$adherent->id}}">{{$adherent->prenom}}  {{$adherent->nom}}</option>


                                                        @endforeach

                                                    </select>
                                                    @error('adherent')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div style="margin-top: 3%">
                                                    <button type="submit" class="btn btn-primary btn-rounded">Enregistrer</button>
                                                </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
    </div>
<!-- #/ container -->



@endsection
