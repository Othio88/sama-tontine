

@extends('layouts.dashboard')


@section('Content')
<div class=" pt-2 mt-3" >
    <div class="toolbar container-fluid" role="toolbar">
            @if($tontines)
            <h3 class="text-center">Les participants du Tontine :&nbsp;{{$tontines->nomtontine}}</h3>
            @endif
    </div>



              <table class="table align-items-center mb-20">
                <thead>
                  <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Photo profil</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Prenom</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nom</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Adresse</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email</th>
                    <!--<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">date fin</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre echeance</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Versement</th>
                    <th class="text-secondary opacity-7"> </th>-->
                  </tr>
                </thead>
                <tbody>





                    @foreach ($participants as $participant)


                  <tr>

                    @if ($participant->image == null)
                    <td>
                        <img height="50px"  src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="profile_image" class="w-25 border-radius-lg shadow-sm">       </td>
                    @else

                    <td>
                        <img height="50px"  src="{{ asset('images-profil/'. $participant->image) }}" alt="profile_image" class="w-25 border-radius-lg shadow-sm">
                    </td>
                    @endif
                    <td >
                        <h6 class="mb-0 text-sm"> {{$participant->prenom}}
                    </td>


                    <td>
                        <h6 class="mb-0 text-sm"> {{$participant->nom}}
                    </td>
                    <td  class="align-middle text-center text-sm">
                        <h6 class="mb-0 text-sm"> {{$participant->adresse}}
                    </td>

                    <td class="align-middle text-center text-sm">
                        <h6 class="mb-0 text-sm"> {{$participant->email}}
                    </td>




                  </tr>
                  @endforeach

                </tbody>
              </table>
</div>
@endsection




