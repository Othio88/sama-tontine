
@extends('layouts.dashboard')

@section('Content')
<div class=" pt-3 mt-5" >
    <div class="toolbar container-fluid" role="toolbar">

            <h3 class="text-center">Les Tontines disponibles</h3>

    </div>






              <table class="table align-items-center mb-20">

                <thead>
                  <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"></th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nom Tontine</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Periodicité</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">date debut</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre echeance</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Versement</th>

                  </tr>
                </thead>


                <tbody>
                    @foreach($tontinesdispo as $tontine)
                  <tr>

                    <td>
                        <div class="d-flex px-0 py-1">
                            @if ($tontine->imagetontine == Null)
                                <div  class="avatar me-3">
                                    <img src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="kal" class="border-radius-lg shadow">
                                </div>
                            @else
                              <div class="avatar me-3" >
                                    <img src="{{ asset('images-profil/'. $tontine->imagetontine) }}" alt="kal" class="border-radius-lg shadow">

                                </div>
                            @endif
                          <div class="d-flex flex-column justify-content-center">

                          </div>
                        </div>
                      </td>

                    <td>
                      <div class="mb-0 text-sm">
                        <div>
                            <h6 class="mb-0 text-sm"> <span>{{$tontine->nomtontine}}</span>
                        </div>
                      </div>
                    </td>


                    <td>
                        <h6 class="mb-0 text-sm"> {{$tontine->periodicite}}
                    </td>
                    <td class="align-middle text-center text-sm">
                        <h6 class="mb-0 text-sm"> {{$tontine->dateDeb}}
                    </td>



                    <td class="align-middle text-center">
                        <h6 class="mb-0 text-sm">{{$tontine->nb_echeance}}
                    </td>
                    <td class="align-middle text-center">
                        <h6 class="mb-0 text-sm">{{$tontine->versement}} Fcfa
                    </td>


                  <td class="align-middle">
                    <a href="/adherent/adhesion-tontine/{{$tontine->id}}" class="btn bg-gradient-success mt-3 btn-sm" >
                      Adherer
                    </a>
                  </td>



                  <td class="align-middle">

                  </td>




                  </tr>
                  @endforeach


                </tbody>


              </table>


</div>
@endsection




