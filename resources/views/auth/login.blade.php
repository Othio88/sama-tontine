@extends('layouts.app')



@section("Content")

<h3 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h3>
<div class="container">
    <div class="row justify-content-center">
                <div class="card-body">
                    <div>
                        <form role="form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <label>Email</label>
                            <div class="mb-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label>Mot de passe</label>
                            <div class="mb-3">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : ""}}>
                              <label class="form-check-label" for="rememberMe">Se souvenir de moi!</label>
                            </div>
                            <div class="text-center">
                              <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Connecter</button>
                            </div>


                            <div style="text-align: center; margin-top:8px" >
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}" >
                                           j'ai oublier mon mot de passe
                                        </a>
                                    @endif
                                </div>
                            </div>

                          </form>

                          <div style="text-align: center; margin-top:8px" >
                            <a class="btn btn-link" href="{{ route('register') }}" >
                                S'Inscrire
                             </a>
                          </div>


                    </div>
                </div>
</div>
@endsection

@section("Contentphoto")

<div class="col-md-6">
    <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
      <div class="oblique-image  position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" heigth="auto" style="background-image:url('../assets/img/curved-images/login.png') ; background-repeat: no-repeat;"></div>
    </div>
  </div>
@endsection







