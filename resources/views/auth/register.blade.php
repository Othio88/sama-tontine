@extends('layouts.app')



@section("Content")

<h3 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h3>

<div class="container">
    <div class="row justify-content-center">
                <div class="card-body">
                    <div>
                        <form role="form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <label>Prenom</label>
                            <div class="mb-3">
                                <input id="prenom" type="prenom" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>

                                @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label>Nom</label>
                            <div class="mb-3">
                                <input id="nom" type="nom" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>

                                @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label>Adresse</label>
                            <div class="mb-3">
                                <input id="adresse" type="adresse" class="form-control @error('adresse') is-invalid @enderror" name="adresse" value="{{ old('adresse') }}" required autocomplete="adresse" autofocus>

                                @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label>Email</label>
                            <div class="mb-3">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            <label>Mot de passe</label>
                            <div class="mb-3">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" autofocus>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label>Confirmer votre mot de passe</label>
                            <div class="mb-3">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>

                            <div class="text-center">
                              <button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">S'inscrire</button>
                            </div>

                          </form>

                    </div>
                </div>
    </div>
</div>
</div>

@endsection
@section('Contentphoto')

<div class="col-md-6">
    <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
      <div class="oblique-image  position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"  style="background-image:url('../assets/img/curved-images/reset1.png');  background-repeat: no-repeat;"></div>
    </div>
  </div>
@endsection




