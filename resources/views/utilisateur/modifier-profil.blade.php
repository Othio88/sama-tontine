@extends("layouts.app")

@section("Content")



<div class="container">

    <div style="text-align: center">
      <h2 class="card-title">Mon compte</h2>

      @if(Auth::user()->image == NULL)
      <img alt="" class="rounded-circle mt-4" width="200px" height="200px"  src="assets/img/curved-images/curved-11.jpg">
      @else
      <img alt="profil image" width="150px" height="200px" class="rounded-circle mt-4" src="{{ asset('images-profil/'. Auth::user()->image) }}">
      @endif
      <h4 class="card-widget__title text-dark mt-3">{{Auth::user()->nom}} {{Auth::user()->prenom}}</h4>
      <p class="text-muted">{{Auth::user()->adresse}} | {{Auth::user()->email}}</p>


      <form action="{{ url('profil-image-update') }}" method="POST" enctype="multipart/form-data">
          @csrf
                      <label>Modifier mon photo de profil</label>
                      <input class="form-control-file @error('image') is-invalid @enderror" type="file" name="image"/>
                      @error('image')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                      @enderror
                   <div style="margin-top: 3%">
                      <input type="submit" class="btn btn-primary" value="Changer"/>
                   </div>

                   <hr class="horizontal dark my-3">

      </form>
    </div>


  </div>



<div class="row justify-content-center">
    <div class="card-body">


                                <form action="{{ url('profil-update') }}" method="POST">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group ">
                                            <label>Nom</label>
                                            <input type="text" name="nom" class="form-control  @error('nom') is-invalid @enderror" required value="{{Auth::user()->nom}}" placeholder="Nom">
                                            @error('nom')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                        <div class="form-group ">
                                            <label>Prenom</label>
                                            <input type="text" name="prenom" class="form-control  @error('penom') is-invalid @enderror" required value="{{Auth::user()->prenom}}" placeholder="Prenom">
                                            @error('prenom')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group ">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" required value="{{Auth::user()->email}}" placeholder="Email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                        <div class="form-group ">
                                            <label>Adresse</label>
                                            <input type="text" name="adresse" class="form-control  @error('adresse') is-invalid @enderror"  required value="{{Auth::user()->adresse}}" placeholder="Password">
                                            @error('adresse')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Mot de passe</label>
                                        <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" required>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-rounded">Enregistrer</button>
                                </form>




    </div>
    </div>


@endsection

@section('Contentphoto')


 <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('../assets/img/curved-images/curved-6.jpg')"></div>
              </div>
            </div>

@endsection
