
@extends('layouts.app')

@section('contentPage')

           <div class=" pt-0 mt-0" >




<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="assets/img/curved-images/p1.png" class="d-block w-100  " height="700" alt="..." >
        <div class="carousel-caption d-none d-md-block" style="margin-bottom: 15%;">
            <h1 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h1>
          <p>Une Sama-tontine est un système d'épargne communautaire dans lequel un groupe de personnes contribue à une cagnotte régulièrement et le montant total est remis à un membre du groupe chaque mois ou chaque année.</p>
        </div>
      </div>

      <div class="carousel-item active">
        <img src="assets/img/curved-images/p2.png" class="d-block w-100" height="700" alt="...">
        <div class="carousel-caption d-none d-md-block" style="margin-bottom: 15%;">
            <h1 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h1>
          <p>Une Sama-tontine est un système d'épargne communautaire dans lequel un groupe de personnes contribue à une cagnotte régulièrement et le montant total est remis à un membre du groupe chaque mois ou chaque année.</p>
        </div>
      </div>

      <div class="carousel-item active">
        <img src="assets/img/curved-images/p3.png" class="d-block w-100" height="700" alt="...">
        <div class="carousel-caption d-none d-md-block" style="margin-bottom: 15%;">
            <h1 class="font-weight-bolder text-info text-gradient">YekSil Cii "SAMA-TONTINE"</h1>
          <p>Une Sama-tontine est un système d'épargne communautaire dans lequel un groupe de personnes contribue à une cagnotte régulièrement et le montant total est remis à un membre du groupe chaque mois ou chaque année.</p>
        </div>

      </div>

    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>


  </div>





  <div class="container-fluid py-4" style="margin-top: 2% ; text-align:center">
    <div class="row">
      <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Tontine Journalier</p>
                 @if ($nombre1 == Null)

                 <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder"> 0</span>
                  </h5>
                  @else
                 <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">{{$nombre1}}</span>
                  </h5>
                 @endif
                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Tontine hebdomadaire</p>
                  @if ($nombre2 == Null)
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">0</span>
                  </h5>
                  @else
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">{{$nombre2}}</span>
                  </h5>
                  @endif
                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Tontine Mensuelle</p>
                  @if ($nombre3 === Null)
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">0</span>
                  </h5>
                  @else
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">{{$nombre3}}</span>
                  </h5>
                  @endif
                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-sm-6">
        <div class="card">
          <div class="card-body p-3">
            <div class="row">
              <div class="col-8">
                <div class="numbers">
                  <p class="text-sm mb-0 text-capitalize font-weight-bold">Tontine Annuelle</p>
                  @if ($nombre4 === Null)
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder"> 0</span>
                  </h5>
                  @else
                  <h5 class="font-weight-bolder mb-0">

                    <span class="text-success text-xl font-weight-bolder">{{$nombre4}}</span>
                  </h5>
                  @endif
                </div>
              </div>
              <div class="col-4 text-end">
                <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                  <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <P style="margin-top:3%"><HR NOSHADE></P>
                   <div style="text-align:center; margin-top:2%"> <h1>Bienvenue dans SamaTontine</h1></div>


                   <P style="margin-bottom:1%"><HR NOSHADE></P>




    <div class="row my-4">
        <div class="col-lg-8 col-md-6 mb-md-0 mb-4">
          <div class="card">
            <div class="card-header pb-0">
              <div class="row">
                <div class="col-lg-6 col-7">
                  <h6>Les Tontines</h6>
                  <p class="text-sm mb-0">
                    <i class="fa fa-check text-info" aria-hidden="true"></i>
                   @if($nbr)
                   <span class="text-success text-sm font-weight-bolder">{{$nbr}}</span>@endif qui existe pour l'instant dans la base de donnees
                  </p>
                </div>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nom Tontine</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Periodicite</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">date de debut</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Versement</th>
                      <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Nombre Echeance</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tontines as $tontine )
                    <tr>

                      <td>
                        <div class="d-flex px-2 py-1">
                            <div class="icon icon-shape bg-gradient-success shadow text-center border-radius-md">
                                <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                              </div>
                          <div class="d-flex flex-column justify-content-center"  style="margin-left: 3%">
                            <h6 class="mb-0 text-sm">{{$tontine->nomtontine}}</h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="avatar-group mt-2">
                            <div class="d-flex flex-column justify-content-center">
                                <h6 class="mb-0 text-sm">{{$tontine->periodicite}}</h6>
                              </div>
                        </div>
                      </td>

                      <td class="align-middle text-center text-sm">
                        <span class="text-xs font-weight-bold">{{$tontine->dateDeb}}</span>
                      </td>

                      <td class="align-middle text-center text-sm">
                        <span class="text-xs font-weight-bold">{{$tontine->versement}}</span>
                      </td>

                      <td class="align-middle text-center text-sm">
                        <span class="text-xs font-weight-bold">{{$tontine->nb_echeance}}</span>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


        <div class="col-12 col-xl-4">
            <div class="card h-100">
              <div class="card-header pb-0 p-3">
                <h3 class="mb-0">Les Utilisateurs</h3>
                @if ($nbrUser)
                <span class="text-success text-sm font-weight-bolder">{{$nbrUser}}</span> utilisateurs
                @endif
              </div>
              <div class="card-body p-3">
                <ul class="list-group">
                    @foreach ( $users as $user )
                  <li class="list-group-item border-0 d-flex align-items-center px-0 mb-2">
                    <div class="avatar me-3">
                        @if ($user->image == null)
                        <img height="50" width="10" src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="kal" class="border-radius-lg shadow">
                    @else
                       <img height="50" width="10" src="{{ asset('images-profil/'. $user->image) }}" alt="kal" class="border-radius-lg shadow">
                    @endif

                    </div>
                    <div class="d-flex align-items-start flex-column justify-content-center">
                      <h6 class="mb-0 text-sm">{{$user->prenom}}&nbsp;{{$user->nom}}</h6>
                      <p class="mb-0 text-xs">Utilisateur de Sama-Tontine</p>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>


          <P style="margin-top:3%"><HR NOSHADE></P>
                               <h1 style="text-align: center ; margin-top:3%" class="font-weight-bolder text-dark text-gradient">Les Tontines en Image</h1>
                                     <P style="margin-bottom:3%"><HR NOSHADE></P>

    <div style="text-align: center">
          <div class="row" style="margin-top:5% ;">
            <div class="col-lg-13">
              <div class="owl-carousel owl-services" >
                @foreach ( $tontinesCreer as $tontine )
                <div class="item">
                  <h4> Nom  :&nbsp;<span class="text-success text-xl font-weight-bolder"> {{$tontine->nomtontine}}</span></h4>
                  <a href="/adherer/tontine-selectioner/{{$tontine->id}}">
                  @if($tontine->imagetontine == Null)

                            <img height="6" width="8" src="{{url('assets/img/curved-images/curved-11.jpg')}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
                            @else

                            <img height="6" width="8" src="{{ asset('images-profil/'. $tontine->imagetontine) }}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">

                    <p></p>
                  @endif
                </a>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>





<!-- Button trigger modal -->

  <!-- Modal -->





              <!-- Scrollable modal -->





              <!-- Button trigger modal -->
           </div>
@endsection











